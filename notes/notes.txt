* templating

* forms 

* email

* static files

* bootstrap: 
	go to getbootstrap.com....getting started
	click on examples
	download static top navbar example
	view source
	copy the code and create base.html inside templates folder
	paste the copied code
	and visit http://127.0.0.1:8000
	you will see that content is not rendering properly

	So from getbootstrap.com...copy the related css & js files link from CDN section and paste then refresh the page.
	------
			<!-- Latest compiled and minified CSS -->
			<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

			<!-- Optional theme -->
			<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

			<!-- Latest compiled and minified JavaScript -->
			<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	------	
	Now you will see the properly rendered page
-  Comment the above links...we wanna download/save the code from view soure(css & js)
	From view source...
	*copy the code for bootstrap.min.css and save it inside Source/static_as_source_sibling/our_static/css/
	*Also save the navbar-static-top.css

- include => {% load staticfiles %} at very top of base.html
- modify links
	eg. like this <link href="{% static 'navbar-static-top.css' %}" rel="stylesheet">
					<link href="{% static 'bootstrap.min.css' %}" rel="stylesheet">

- Refresh page...nice look...but we forgot to include some js ...so include that
  because CSS is related to design so it is looking nice but not fully functional.

- 
* DTL:
  base.html==dtl_base.html
  ------------------------
- Cut navbar code...and paste it in navbar.html
  In place of navbar code ...in dtl_base.html

  paste these => {% include "navbar.html" %}


* crispy forms...
	-- http://django-crispy-forms.readthedocs.io/en/latest/install.html
	--- pip install --upgrade django-crispy-forms 

	-- Add crispy_forms to INSTALLED_APPS

	--- python manage.py makemigrations
	--- python manage.py migrate

	In settings.py(at very bottom)
	-- CRISPY_TEMPLATE_PACK ="bootstrap3"

	-- go to template and => {% load crispy_forms_tags %}

	-- Replace => form.as_p  with => form|crispy


* bootstrap grid system(21...)

* CSS blocks

* URLs as links:
	<a href="{{ url 'about' }}">About</a>  <!--name of view is about-->
  - If we will click ont the above link that it will take us to the related page.
  - co-md-offset-3 etc.


* 	- Django Registration Redux
	- visit https://django-registration-redux.readthedocs.io/en/latest/ & read it
	- Download the code from joincfe/gihub   ... trydjango18 for copying the templates from  
	  templates/registration
	- Download => https://github.com/codingforentrepreneurs/Try-Django-1.8 
	- Run => cp -R ~/projects/Python/Django/Downloaded/Try-Django-1.8aster/src/templates/registration/* ./templates/registration/

	- pip install django-registration-redux
	- add => 'registration' to INSTALLED_APPS(3rd party)
	- Also add => 'django.contrib.sites' to INSTALLED_APPS(django apps)
	- Add => ACCOUNT_ACTIVATION_DAYS = 7 (or any other) 
	- Add => REGISTRATIO_AUTO_LOGIN = True, at very bottom of the settings.py

	- Copy => (r'^accounts/', include('registration.backends.default.urls')), from site & add it to urls.py 

	- We don't need to worry about the templates mentioned in the doc...as we have already downloaded it.

	- So now run => python manage.py migrate
	- python manage.py runserver
	- visit accounts/register (If we are not following the video as it is then we may need to remove some code stuffs...as I removed "{% url 'about' %} etc.)

	- A nice registration will be in front of you.
	- Open registration_form.html and load => crispy_forms_tags.
	- Add => form|crispy

	- Add SITE_ID=1 to settings.py before vising /accounts/login/

	- Visit /admin/......you will find => 'sites' link in this page.
	- Here we can set the domain name, display name etc.

	- Go through different pages inside registration dir...

* Updating django Login URL to Custom URL(Very short video ... 1:24 minutes)
	- Just set => LOGIN_REDIRECT_URL = '/blogs/' (or any other)
	- And login at '/accounts/login/'
	- it will take you to '/blogs/' after login.

* Authentication links in navigation bar(At this point I had alreay done that)
	{% if request.user.is_authenticated %}
			<a href='{% url 'auth_logout' %}'>Logout</a>	
	{% else %}
			<a href='{% url 'auth_login' %}'>Login</a>
			<a href='{% url 'auth_register' %}'>Register</a>
	{% endif %}

	- if we will try to visit Register page(some errors can come...Reverse for auth_register...etc)

	- So google this proble => django registration redux code => github => registration => backends => default => urls.py

	- You will find that actual url name is 'registration-register'
	- So replace 'auth_register' with 'registration_register'


* Login Form in Bootstrap Navbar
	- See what is => {% if not requsest.user.is_authenticated and if not "/accounts/login" in request.get_full_path %}

* Styling MVP Landing Part 3
	- Arranging the html elements properly at proper places by inspecting...etc.
	- Style the navbar by using bootstrap.
	- Jubotron background etc...

* Logo Image in Navbar(Short video...Logo Image[MVP Landing])
	- Add image logo on if you want

* Promo video & images
	- Go to youtube...click on embed.
	- Copy the iframe html element code
	- Use it in your site wherever you want.
	- You can also use images link...(I had already done this before watching this...)

* Icons with Font Awesome
	- Use https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css as a CDN link to use awesome font on the site...
	- You can also download to use it locally.
	- visit fontawesome.io and seet the using/including syntax...
	eg...
	---
		<i class="fa fa-camera-retro fa-lg"></i> fa-lg
		<i class="fa fa-camera-retro fa-2x"></i> fa-2x
		<i class="fa fa-camera-retro fa-3x"></i> fa-3x
		<i class="fa fa-camera-retro fa-4x"></i> fa-4x
		<i class="fa fa-camera-retro fa-5x"></i> fa-5x
	---
	- I also used some awesomw fonts in my site.
	










