>>> import sqlite3
>>> db=sqlite3.connect("db.sqlite3")
>>> rows=db.execute("select name from sqlite_master where type='table'")
>>> print rows.fetchall()
[(u'django_migrations',), (u'sqlite_sequence',), (u'auth_group',), (u'auth_group_permissions',), (u'auth_user_groups',), (u'auth_user_user_permissions',), (u'django_admin_log',), (u'django_content_type',), (u'auth_permission',), (u'auth_user',), (u'django_session',), (u'HyGoApp_signup',)]
>>> cursor=db.execute("select * from HyGoApp_signup")
>>> print cursor
<sqlite3.Cursor object at 0x10cc22ab0>
>>> for row in cursor:
...     print row[0],row[1]
... 
1 rishikesh0014051992@gmail.com
2 hemkesh@gmail.com
3 malinikesh@gamil.com
4 darshan@gmail.com
5 pratik@gmail.com
6 prajapati@gmail.com
7 vinit@gmail.com
8 shivesh@gmail.com
9 feeroz@gmail.com
10 cuty@gmail.com
11 dolly@gmail.com
12 prince@gmail.com
13 rishikesh@synkku.com
14 filter@gmail.com
15 del@synkku.com
16 ok@gmail.com
17 fire@gmail.com
18 fish@gmail.com
19 fool@gmail.com
20 rob@gmail.com
21 tiger@gmail.com