#Custom form in view

class LoginForm(forms.Form):
	username=forms.CharField(max_length=20)
	email=forms.EmailField(max_length=30)

	def clean_username(self):
		signup_dict=self.cleaned_data
		print "Login Details : ",signup_dict
		username=signup_dict["username"]
		lst=re.findall(r"^([A-Z]{1})([a-z]{2,19})$",username)
		print "Matched : ",lst
		if not len(lst)==1:
			print "Not matched..."
			raise forms.ValidationError("First letter of username should be in capital followed by 2 to 19 small case letters")
		return username
