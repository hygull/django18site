>>> import sqlite3
>>> db=sqlite3.connect("db.sqlite3")
>>> cursor=db.execute("select name from sqlite_master where type='table'")
>>> print cursor.fetchall()
[(u'django_migrations',), (u'sqlite_sequence',), (u'auth_group',), (u'auth_group_permissions',), (u'auth_user_groups',), (u'auth_user_user_permissions',), (u'django_admin_log',), (u'django_content_type',), (u'auth_permission',), (u'auth_user',), (u'django_session',), (u'HyGoApp_signup',)]