Images, Javascript & CSS are known as static files in Django.

They are served by other server.

If we will bring it into production, we will have 2 servers.
1 server for Django related stuffs
2 server for static files like Images, Javascript & CSS.

Check in settings.py
---
1) whether django.contrib.staticfiles is already in INSTALLED_APPS tuple or not.

2) define STATIC_URL ="/static/" if it is not there(very bottom)...the path to files to be served

There are number of ways to place "static" folders...
1) Create a folder inside Source/src(Main project folder in my case)...give name as static_as_source_child
2) Create another folder in level of Source/src ...give name as static_as_source_sibling

Define STATICFILES_DIRS Tuple...

STATICFILES_DIRS=( #Don't give any other name (you won't get any error but files won't be served)
	os.path.join("BASE_DIR","static_as_source_child"),
)

-----
Define STATIC_ROOT (We the served files will go)

Create 2 new folders inside static_as_source_child
1...our_static (We will put staticfiles)
2...static_root (Files will be served here)

Change STATICFILES_DIRS as follows:
STATICFILES_DIRS=(
	os.path.join(BASE_DIR,"static_as_source_child","our_static"),
)

and SET STATIC_ROOT as follows:

STATIC_ROOT=os.path.join(BASE_DIR,"static_as_source_child","static_root")

Create another folder named img inside our_static 
and create 1 text file(just for testing) there

and run => python manage.py collectstatic

It will create 2 folders inside static_root=> admin(django creates it) and img
----

Here we are serving our files inside the static_as_source_child/static_root....
but this is not a proper place to serve.

Serve it outside the projects folder.

Let's change the previous settings...

STATIC_ROOT=os.path.join(os.path.dirname(BASE_DIR),"static_as_source_sibling","static_root")

Now run => python manage.py collectstatic

----
Now define media settings for profile pic etc.
It is manual process unlike the static one....
....
MEDIA_URL ="/media/"
MEDIA_ROOT=os.path.join(os.path.dirname(BASE_DIR),"static_as_source_sibling","media_root")
*******************

Now open urls.py(to serve media etc)

and do 2 imports
----
from django.conf import settings
from django.contrib.urls.static import static

and do an append...
---urls.py 
from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings #Added
from django.contrib.urls.static import static #Added 
urlpatterns = [
    # Examples:
    url(r'^$', 'HyGoApp.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^login/$', 'HyGoApp.views.login', name='login'),
     url(r'^login2/$', 'HyGoApp.views.login2', name='login2'),
    url(r'^submit/$', 'HyGoApp.views.success', name='success'),
    url(r'^error/$', 'HyGoApp.views.error', name='error'),
    url(r'^admin/', include(admin.site.urls)),

    url(r"^blogs/","HyGoApp.views.blogs",name="blogs"),
    url(r"^aboutus/","HyGoApp.views.aboutus",name="aboutus"),
]+static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
-------
but this append is good only for development(its not good for production)...

But its not good to directly append it to it...So if DEBUG is True then add.

----remove the append and add the below code at very bottom of urls.py
if settings.DEBUG:
	urlpatterns+=static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
	urlpatterns+=static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
----

Now run the server & visit.../admin/

inspect element....

You will find, Now static files are coming from /static/css/  ...etc.


